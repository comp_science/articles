## Статьи о shell


[Начинаем новый уровень — терминал. 
Текстовая среда для управления компутером.](https://comp-science.xyz/?go=all/term/)

[Папки, перемещение и создание файлов в терминале.](https://comp-science.xyz/?go=all/file-system/)

[Описание работы команд и расширение их опций.](https://comp-science.xyz/?go=all/manuals-n-options/)

[Посмотреть, найти или написать в файле через терминал.](https://comp-science.xyz/?go=all/head-tail-grep/)

[Пользователи терминала и их права.](https://comp-science.xyz/?go=all/sudo/)

[Поставить, проверить, догрузить.](https://comp-science.xyz/?go=all/console-soft/)