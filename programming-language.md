[Вводная статья про си, си++ и пайтон.
Выбираем язык для следующей статьи](https://comp-science.xyz/?go=all/programming-languages-review/)

[Скачиваем и проверяем пайтон. 
Выводим строку с текстом.](https://comp-science.xyz/?go=all/hello-python/)

[Чтобы было удобно писать код и знать как правильно его писать.](https://comp-science.xyz/?go=all/syntax-and-editor/)

[Пишем свои функции и пользуемся библиотеками из интернета.](https://comp-science.xyz/?go=all/work-func/)

[Три типа данных, четыре логических операции и два цикла.](https://comp-science.xyz/?go=all/data-type-condition-cycle/)

[Тип для символов, функции для составных типов данных и чуть-чуть про цикл for.](https://comp-science.xyz/?go=all/string-and-component-data-types/)

[Программа не работает, да ещё и ничего не понятно.](https://comp-science.xyz/?go=all/errors-and-exceptions/)

[Из чего состоит код. Стили описания задач.
Тесты для логических ошибок.](https://comp-science.xyz/?go=all/expression-and-instruction/)

[Разделы видимости, фокусы с функциями и их разновидности.](https://comp-science.xyz/?go=all/env-and-closure/)

[Функция вызывает сама себя. 
Виды рекурсии.](https://comp-science.xyz/?go=all/recursive-and-iterative/)

[Делаем себе бота.
Пишем каркас и запускаем.](https://comp-science.xyz/?go=all/bot/)

[Переборщик прокси для бота.
Импорт функций из другого файла, запись и чтение из TXTшников.](https://comp-science.xyz/?go=all/proxy-changer/)

[Сохраняем отчёты об ошибках и вживляем бота в ОСку.](https://comp-science.xyz/?go=all/big-whale/)

[Арендуем сервер и заселяем туда бота.](https://comp-science.xyz/?go=all/storage-imagery/)

[Первая статья про алгоритмы: что такое, графики и как считать память](https://comp-science.xyz/?go=all/intro-to-algorithms-n-math/)

[Отдельная статья про матрицы, для всех кто еще не проникся важностью предмета](https://comp-science.xyz/?go=all/matrix-in-math/)

[Начинаем оживлять бота: подключаем ИИ и обучаем его.](https://comp-science.xyz/?go=all/connect-to-dialogflow/)